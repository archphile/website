+++
title =  "Download"
menu = "main"
weight = "3"
+++

Archphile currently supports all **ARM7 Raspberry Pis** and the **Odroid C2**. The latest version is **0.99.73 alpha**.




[latest image for the Raspberry Pi 2/3/3 B+](https://sourceforge.net/projects/archphile/files/rpi3/0.99.73-alpha-rpi23/archphile-0.99.73-alpha-rpi23.7z/download) | [changelog](https://archphile.org/changelog/rpi.txt)

[latest image for the Odroid C2](https://sourceforge.net/projects/archphile/files/odroidc2/0.99.73-alpha/archphile-0.99.73-alpha-odroidc2.7z/download) | [changelog](https://archphile.org/changelog/odroidc2.txt)
