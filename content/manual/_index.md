+++
title = "Manual"
menu =  "main"
weight = "4"
+++
Archphile, not having a gui for its configuration, is always a pain in the ass for users that don’t have any idea about Linux.

Since the beginning of this project, I tried to provide as much as information I could, mainly via the old website's Tips & Tricks section.

That was not enough and this is why since May 2018 I introduced the Archphile manual:

[Archphile manual v 0.12.12](https://archphile.org/manual/archphile-manual.pdf) 

This document is still under development, but it currently includes almost everything you need in order to configure and use Archphile.

If you want to get and editable (.odt) version of this manual, you can visit the following link:

[Archphile manual Github repository](https://github.com/archphile/manual) 

&nbsp;
**Notes:**

- Archphile manual is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

-  Experienced Linux users will immediately notice the same systemd commands (plus some nano editing) to be repeated again and again. Please note that this document was created for people that don’t have any idea about Linux and may need to copy/paste commands in order to make their board functional.
