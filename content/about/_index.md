+++
title = "About"
menu = "main"
weight = "1"
+++

Archphile is an [ArchlinuxARM](https://archlinuxarm.org/) based distribution for the [Raspberry Pi](https://www.raspberrypi.org/) and the [Odroid C2](https://www.hardkernel.com/main/products/prdt_info.php?g_code=G145457216438).

**Features:**

- [MPD](https://www.musicpd.org/)  (Music Player Daemon)
- [mympd](https://github.com/jcorporation/myMPD)/[ympd](https://github.com/notandy/ympd)  MPD clients
- UPNP/DLNA support using [upmpdcli](https://www.lesbonscomptes.com/upmpdcli/)
- spotify support using [librespot](https://github.com/librespot-org)
- tidal support using [upmpdcli](https://www.lesbonscomptes.com/upmpdcli/) 
- airplay support using [shairport-sync](https://github.com/mikebrady/shairport-sync) 
- usb disk automounting using [udevil](https://ignorantguru.github.io/udevil/)
- samba/cifs/nfs support
- various webradios included by default (heavily based on  [moodeaudio](http://moodeaudio.org/) webradios)


Below you will find some screenshots showing the MPD clients currently included in Archphile:

- **ympd**

![ympd](/img/ympd.jpg  "YMPD screenshot")


- **mympd** (coming very soon..)

![mympd](/img/mympd.gif  "Mympd screenshots")

**Note:** Archphile was initially based on ideas and configuration from **Raspyfi/Volumio** and **Runeaudio** projects. I could not have done it without them.

**Credits:**

- https://archlinuxarm.org
- https://www.musicpd.org
- https://www.ympd.org
- https://github.com/jcorporation/myMPD
- https://volumio.org
- http://www.runeaudio.com
- http://www.raspyfi.com
- http://moodeaudio.org

This website was created using the following software:

- [hugo framework](https://gohugo.io/)
- [manis hugo theme](https://github.com/yursan9/manis-hugo-theme) 
